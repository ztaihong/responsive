## 响应式Web开发

　　根据中国互联网络信息中心（CNNIC）发布第的39次《中国互联网络发展状况统计报告》，截至2016年12月，我国网民总规模达7.31亿，其中手机网民规模达6.95亿，增长率连续三年超过10%，台式电脑、笔记本电脑的使用率均出现下降，手机不断挤占其它个人上网设备。

　　当今的网站设计必须考虑用户上网所使用终端的差异性，一个网站不但要在桌面计算机的显示器上渲染展示良好，还必须在平板、手机等移动终端上完美显示，而且必须考虑支持各种浏览器，这就是响应式Web开发所要达到的终极目标。

### 一、开发工具

* Borwser Sync
* Sublime Text3

### 二、技术标准

* HTML5
* CSS
* JavaScript
* Twitter Bootstrap

### 三、代码托管

https://gitlab.com/userName/responsive
> 注意：请使用你自己在gitlab注册的用户名代替userName

### 四、网站托管

https://userName.gitlab.io/responsive
> 注意：请使用你自己在gitlab注册的用户名代替userName